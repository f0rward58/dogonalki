package Dogonalki;

/**
 * Created by maksi on 06.11.2017.
 */
public class Dogonalki extends Thread {
    public void run() {
        Thread thread = Thread.currentThread();
        thread.setPriority(1);

        for (int i = 0; i < 100; i++) {
            try {
                sleep(1);
            } catch (InterruptedException e) {
            }

            System.out.println("Поток1 -" + i);
            if (i == 32) {
                thread.setPriority(10);
            }
        }
    }
}

class Demo {

    public static void main(String[] args) {

        Dogonalki dogonalki = new Dogonalki(); //Создание потока
        Thread thread = Thread.currentThread();
        thread.setPriority(10);
        dogonalki.start(); //Запуск потока

        for(int i = 0; i < 100; i++) {

            try {
                Thread.sleep(1); //Приостанавливает поток на 1 секунду
            } catch(InterruptedException e){}

            System.out.println("Поток 2 -" +i);
            if(i==32){
                thread.setPriority(1);
            }
        }

    }
}
